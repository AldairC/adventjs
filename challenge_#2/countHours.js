// Algorithm to count extra hours in the year
function countHours(year, holidays) {
  return holidays
    .map((holiday) => {
      let day = new Date(`${holiday}/${year}`).getDay();
      let isJobDay = day > 0 && day < 6;
      return isJobDay ? 2 : 0;
    })
    .reduce((a, b) => a + b, 0);
}
