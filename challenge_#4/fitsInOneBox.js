// Fits in one box algorithm
function fitsInOneBox(boxes) {
  const sortBoxes = boxes.map((box) => Math.min(...Object.values(box)));
  return new Set(sortBoxes).size === sortBoxes.length;
}
