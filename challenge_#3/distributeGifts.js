// Distribute gifts algorithm
function distributeGifts(packOfGifts, reindeers) {
  let weightPackOfGifts = packOfGifts.join("").length;
  let weightReindeers = reindeers.join("").length * 2;
  return parseInt(weightReindeers / weightPackOfGifts);
}
